package org.codehaus.mojo.license.model;


import java.util.Arrays;
import java.util.List;

public class NonMavenDependency extends Dependency
{
    private final String name;
    private String URL;
    private final String version;
    private String license;


    public NonMavenDependency(String name, String version)
    {
        this.name = name;
        this.version = version;

    }

    public String getLicense()
    {
        return license;
    }

     public String getId(){
        return  name + ":" + version;
    }

    public String getName()
    {
        return name;
    }
    public String getVersion()
    {
        return version;
    }
    public String getUrl()
    {
        return URL;
    }

    public void setURL(String URL)
    {
        this.URL = URL;
    }

    public void setLicense(String licenseName)
    {
        this.license = licenseName;
    }


    public String getArtifactName()
    {
        return name + "--" + version;
    }

    public String getArtifactScope()
    {
        return "runtime";
    }

    @Override
    public boolean isComplete()
    {
        return license != null && !getScopesPerModule().isEmpty();
    }

    @Override
    public List<License> getLicenses()
    {
        License license = new License();
        license.setName(this.license);
        return Arrays.asList(license);
    }

    @Override
    public String getGAVInfo()
    {
        return version;
    }
}
