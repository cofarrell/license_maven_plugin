package org.codehaus.mojo.license.model;


import org.apache.maven.project.MavenProject;
import org.codehaus.mojo.license.utils.MojoHelper;

import java.util.List;

public class MavenDependency extends Dependency
{

    private MavenProject mavenProject;


    public MavenDependency(MavenProject parentProject, MavenProject mavenProject, Scope moduleScope)
    {
        if (getLicenseScope() == Scope.IGNORE )
            throw new IllegalArgumentException("You should not create a dependency with this scope");

        this.mavenProject = mavenProject;
        this.addScopeOnModule(parentProject, calculateLicenseScope(mavenProject.getArtifact().getScope(), moduleScope));
    }



    private Scope calculateLicenseScope(String artifactScope, Scope moduleScope)
    {

        if (moduleScope == Scope.BINARY
                && !artifactScope.equals("test") )
        {
            return Scope.BINARY;
        }
        else
        {
            return Scope.TEST;
        }
    }


    public boolean hasLowerScope(String artifactScope, Scope moduleScope)
    {
        return this.getLicenseScope() == Scope.TEST
                && calculateLicenseScope(artifactScope, moduleScope) == Scope.BINARY;
    }

    public String getArtifactName()
    {
        return MojoHelper.getArtifactId(mavenProject.getArtifact());
    }

    public String getName()
    {
        return mavenProject.getName();
    }

    public String getId()
    {
        return mavenProject.getId();
    }

    public String getUrl()
    {
        return mavenProject.getUrl();
    }

    public MavenProject getMavenProject()
    {
        return mavenProject;
    }

    public String getArtifactScope()
    {
        return mavenProject.getArtifact().getScope();
    }


    public List<License> getLicenses()
    {
        return mavenProject.getLicenses();
    }

    @Override
    public String getGAVInfo()
    {
        return getId();
    }

    @Override
    public boolean isComplete()
    {
        return mavenProject != null && !getScopesPerModule().isEmpty();

    }
}
