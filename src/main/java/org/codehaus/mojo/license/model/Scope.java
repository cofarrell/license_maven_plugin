package org.codehaus.mojo.license.model;

public enum Scope
{
    BINARY (false), TEST (false), IGNORE (true);

    private boolean restricted;

    private Scope(boolean restricted)
    {
        this.restricted = restricted;
    }

    public static Scope valueOfIgnoreCaseUnrestricted(String name)
    {
       return valueOfIgnoreCase(name, true);
    }

    public static Scope valueOfIgnoreCaseRestricted(String name)
    {
        return valueOfIgnoreCase(name, false);
    }

    public String getPrintableName()
    {
        return name().toLowerCase();
    }



    public static String getUnrestrictedValues()
    {
       return getValues(true);
    }

    public static String getRestrictedValues()
    {

        return getValues(false);
    }


    private static Scope valueOfIgnoreCase(String name, boolean getAllValues)
    {
        try
        {
            Scope scope = Scope.valueOf(name.trim().toUpperCase());

            if (!getAllValues && scope.restricted)
            {
                throw new IllegalArgumentException();
            }

            return scope;
        }
        catch (IllegalArgumentException e)
        {

            String possibleValues = "";
            if (getAllValues)
            {
                possibleValues = getUnrestrictedValues();
            }
            else
            {
                possibleValues = getRestrictedValues();
            }

            throw new IllegalArgumentException("You cannot use the license scope \"" + name + "\"\nValid values: " + possibleValues);
        }
    }

    private static String getValues(boolean getAllValues)
    {

        StringBuilder values = new StringBuilder();
        for (Scope scope : Scope.values())
        {
            if (getAllValues || !scope.restricted)
            {
                values.append(scope.getPrintableName()).append(",");
            }
        }

        return values.length() > 0? values.toString().substring(0, values.length()-1) : "";
    }

}