package org.codehaus.mojo.license.utils;


import com.google.common.base.Supplier;
import org.apache.maven.artifact.Artifact;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public interface PersistentShaCache
{
    ConcurrentMap<String,Supplier<Artifact>> get();

    void write(Map<String, Artifact> cache);
}
