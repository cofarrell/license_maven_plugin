package org.codehaus.mojo.license.utils;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * @see {@link ArtifactCreator}
 * @plexus.component role="org.codehaus.mojo.license.utils.PersistentShaCache" role-hint="default"
 */
public class FileSystemShaCache implements PersistentShaCache
{
    public static final String CACHE_PATH = StringUtils.join(
            Arrays.asList(System.getProperty("user.home"), ".m2", "license-maven-plugin"), File.separator
    );

    public static final String CACHE_FILENAME = "shaCache";

    /**
     * @plexus.requirement
     */
    private ArtifactFactory artifactFactory;

    private final Gson gson = new Gson();

    private final Supplier<ConcurrentMap<String, Supplier<Artifact>>> fileCache = Suppliers.memoize(new Supplier<ConcurrentMap<String, Supplier<Artifact>>>()
    {
        @Override
        public ConcurrentMap<String, Supplier<Artifact>> get()
        {
            final File cachePathFile = new File(CACHE_PATH);

            if (!cachePathFile.isDirectory())
            {
                return Maps.newConcurrentMap();
            }

            final File cacheFile = new File(cachePathFile, CACHE_FILENAME);
            BufferedReader reader = null;
            try
            {
                reader = new BufferedReader(new FileReader(cacheFile));
                final ShaCache shaCache = gson.fromJson(reader, ShaCache.class);

                final ConcurrentMap<String, Supplier<Artifact>> concurrentMap = Maps.newConcurrentMap();
                for (final ShaCacheEntry shaCacheEntry : shaCache.getShaCacheEntries())
                {
                    if (shaCacheEntry.isEmpty())
                    {
                        concurrentMap.put(shaCacheEntry.getSha(), Suppliers.<Artifact>ofInstance(null));
                    }
                    else
                    {
                        final Artifact artifactWithClassifier = artifactFactory.createArtifactWithClassifier(
                                shaCacheEntry.getGroupId(), shaCacheEntry.getArtifactId(), shaCacheEntry.getVersion(),
                                shaCacheEntry.getType(), shaCacheEntry.getClassifier()
                        );
                        artifactWithClassifier.setScope("compile");
                        concurrentMap.put(shaCacheEntry.getSha(),
                            Suppliers.ofInstance(artifactWithClassifier
                        ));
                    }
                }
                return concurrentMap;
            }
            catch (FileNotFoundException e)
            {
                cacheFile.delete();
                return Maps.newConcurrentMap();
            }
            finally
            {
                IOUtils.closeQuietly(reader);
            }
        }
    });

    @Override
    public ConcurrentMap<String, Supplier<Artifact>> get()
    {
        return fileCache.get();
    }

    @Override
    public void write(final Map<String, Artifact> cache)
    {
        final List<ShaCacheEntry> cacheEntries = Lists.newArrayList();
        for (Map.Entry<String, Supplier<Artifact>> originalShaCache : fileCache.get().entrySet())
        {
            cache.put(originalShaCache.getKey(), originalShaCache.getValue().get());
        }
        for (final Map.Entry<String, Artifact> cacheEntry : cache.entrySet())
        {
            final Artifact artifact = cacheEntry.getValue();
            if (artifact == null)
            {
                cacheEntries.add(new ShaCacheEntry(cacheEntry.getKey()));
            }
            else
            {
                cacheEntries.add(new ShaCacheEntry(cacheEntry.getKey(),
                        artifact.getGroupId(), artifact.getArtifactId(), artifact.getVersion(), artifact.getType(),
                        artifact.getClassifier()));
            }
        }

        final ShaCache shaCache = new ShaCache(cacheEntries);

        final File cachePathFile = new File(CACHE_PATH);
        final File cacheFile = new File(cachePathFile, CACHE_FILENAME);
        if (!cacheFile.exists())
        {
            cachePathFile.mkdirs();
        }

        BufferedWriter cacheFileWriter = null;
        try
        {
            cacheFileWriter = new BufferedWriter(new FileWriter(cacheFile, false));
            gson.toJson(shaCache, cacheFileWriter);
        }
        catch (IOException e)
        {
            cacheFile.delete();
        }
        finally
        {
            IOUtils.closeQuietly(cacheFileWriter);
        }

    }

    private static class ShaCache
    {
        private List<ShaCacheEntry> shaCacheEntries;

        public ShaCache()
        {};

        public ShaCache(final List<ShaCacheEntry> shaCacheEntries)
        {
            this.shaCacheEntries = shaCacheEntries;
        };

        public List<ShaCacheEntry> getShaCacheEntries()
        {
            return shaCacheEntries;
        }
    }

    private static class ShaCacheEntry
    {
        private String sha;
        private String groupId;
        private String artifactId;
        private String version;
        private String classifier;
        private String type;
        private boolean empty;

        private ShaCacheEntry()
        {}

        private ShaCacheEntry(String sha)
        {
            this.sha = sha;
            this.empty = true;
        }

        private ShaCacheEntry(String sha, String groupId, String artifactId, String version,
                              String type, String classifier)
        {
            this.sha = sha;
            this.groupId = groupId;
            this.artifactId = artifactId;
            this.version = version;
            this.classifier = classifier;
            this.type = type;
            this.empty = false;
        }

        public String getSha()
        {
            return sha;
        }

        public String getGroupId()
        {
            return groupId;
        }

        public String getArtifactId()
        {
            return artifactId;
        }

        public String getVersion() {
            return version;
        }

        public String getType()
        {
            return type;
        }

        public String getClassifier()
        {
            return classifier;
        }

        public boolean isEmpty()
        {
            return empty;
        }
    }
}
