package org.codehaus.mojo.license;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuildingException;
import org.codehaus.mojo.license.api.ThirdPartyToolException;
import org.codehaus.mojo.license.model.Dependency;
import org.codehaus.mojo.license.model.LicenseMap;
import org.codehaus.mojo.license.utils.FileUtil;
import org.codehaus.mojo.license.utils.SortedProperties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;

/**
 * Goal to generate the third-party license file.
 * <p/>
 * This file contains a list of the dependencies and their licenses.  Each dependency and it's
 * license is displayed on a single line in the format <br/>
 * <pre>
 *   (&lt;license-name&gt;) &lt;project-name&gt; &lt;groupId&gt;:&lt;artifactId&gt;:&lt;version&gt; - &lt;project-url&gt;
 * </pre>
 * It will also copy it in the class-path (says add the generated directory as
 * a resource of the build).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
@Mojo( name = "add-third-party", requiresProject = true, requiresDependencyResolution = ResolutionScope.TEST,
       defaultPhase = LifecyclePhase.GENERATE_RESOURCES )
public class AddThirdPartyMojo
    extends AbstractAddThirdPartyMojo
{



    // ----------------------------------------------------------------------
    // Private Fields
    // ----------------------------------------------------------------------

    @Parameter( property = "license.generateMissingFile", defaultValue = "false" )
    private boolean doGenerateMissing;


    // ----------------------------------------------------------------------
    // AbstractLicenseMojo Implementaton
    // ----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean checkPackaging()
    {
        if ( isAcceptPomPackaging() )
        {

            // rejects nothing
            return true;
        }

        // can reject pom packaging
        return rejectPackaging("pom");
    }


       /**
     * {@inheritDoc}
     */
    @Override
    protected void doAction()
        throws Exception
    {


        getHelper().removeIgnoredLicenses(licenseMap, getIgnoredLicenses());

        boolean safeLicense = checkForbiddenLicenses();

        writeThirdPartyFile();

        if ( !safeLicense && isFailIfWarning() )
        {
            throw new MojoFailureException( "There is some forbidden licenses used, please check your dependencies." );
        }


        if ( doGenerateMissing )
        {

            writeMissingFile();

            if ( isUseMissingFile() && MapUtils.isEmpty( getOverrideMappings() ) && getOverrideFile().exists() )
            {

                // there is no missing dependencies, but still a missing file, delete it
                getLog().info( "There is no dependency to addDependencies in missing file, delete it at " + getOverrideFile() );
                FileUtil.deleteFile( getOverrideFile() );
            }

            if ( isDeployMissingFile() && MapUtils.isNotEmpty( getOverrideMappings() ) )
            {

                // can deploy missing file
                File file = getOverrideFile();

                getLog().info("Will deploy third party file from " + file);
                getHelper().attachThirdPartyDescriptor( file );
            }

            addResourceDir( getOutputDirectory(), "**/*.txt" );
        }
    }

    // ----------------------------------------------------------------------
    // AbstractAddThirdPartyMojo Implementaton
    // ----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected SortedMap<String,Dependency> loadDependencies()
    {
        return getHelper().loadDependencies( this, getModuleScope(), getIgnoredArtifactsTrees());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SortedProperties createAllOverrideMapping()
        throws ProjectBuildingException, IOException, ThirdPartyToolException
    {


        SortedProperties unsafeMappings =
            getHelper().createOverrideMappingForProject(getLicenseMap(), getModuleScope(), getOverrideFile(), isUseRepositoryMissingFiles(),
                    getProjectDependencies(), getProject());
        if ( isVerbose() )
        {
            getLog().info( "found " + unsafeMappings.size() + " unsafe mappings" );
        }

        // compute if missing file should be (re)-generate
       // doGenerateMissing = computeDoGenerateMissingFile( unsafeMappings, unsafeDependencies );

        if ( doGenerateMissing && isVerbose() )
        {
            StringBuilder sb = new StringBuilder();
            sb.append( "Will use from missing file " );
            sb.append( unsafeMappings.size() );
            sb.append( " dependencies :" );
            for ( Map.Entry<Object, Object> entry : unsafeMappings.entrySet() )
            {
                String id = (String) entry.getKey();
                String license = (String) entry.getValue();
                sb.append( "\n - " ).append( id ).append( " - " ).append( license );
            }
            getLog().info(sb.toString());
        }
        else
        {
            if ( isUseMissingFile() && !unsafeMappings.isEmpty() )
            {
                getLog().debug("Missing file " + getOverrideFile() + " is up-to-date.");
            }
        }
        return unsafeMappings;
    }

    @Override
    protected List<File> getNonMavenDependenciesFiles()
    {
        return Arrays.asList(getNonMavenDependenciesFile());
    }


    // ----------------------------------------------------------------------
    // Private Methods
    // ----------------------------------------------------------------------

    /**
     * @param unsafeMappings     the unsafe mapping coming from the missing file
     * @param unsafeDependencies the unsafe dependencies from the project
     * @return {@code true} if missing ifle should be (re-)generated, {@code false} otherwise
     * @throws IOException if any IO problem
     * @since 1.0
     */
    private boolean computeDoGenerateMissingFile( SortedProperties unsafeMappings,
                                                  SortedSet<MavenProject> unsafeDependencies )
        throws IOException
    {

        if ( !isUseMissingFile() )
        {

            // never use the missing file
            return false;
        }

        if ( isForce() )
        {

            // the mapping for missing file is not empty, regenerate it
            return !CollectionUtils.isEmpty( unsafeMappings.keySet() );
        }

        if ( !CollectionUtils.isEmpty( unsafeDependencies ) )
        {

            // there is some unsafe dependencies from the project, must
            // regenerate missing file
            return true;
        }

        File missingFile = getOverrideFile();

        if ( !missingFile.exists() )
        {

            // the missing file does not exists, this happens when
            // using remote missing file from dependencies
            return true;
        }

        // check if the missing file has changed
        SortedProperties oldUnsafeMappings = new SortedProperties( getEncoding() );
        oldUnsafeMappings.load( missingFile );
        return !unsafeMappings.equals( oldUnsafeMappings );
    }

    /**
     * Write the missing file ({@link #getOverrideFile()}.
     *
     * @throws IOException if error while writing missing file
     */
    private void writeMissingFile()
        throws IOException
    {

        Log log = getLog();
        LicenseMap licenseMap = getLicenseMap();
        File file = getOverrideFile();

        FileUtil.createDirectoryIfNecessary( file.getParentFile() );
        log.info( "Regenerate missing license file " + file );

        FileOutputStream writer = new FileOutputStream( file );
        try
        {
            StringBuilder sb = new StringBuilder( " Generated by " + getClass().getName() );
            List<String> licenses = new ArrayList<String>( licenseMap.getAllLicenses() );
            licenses.remove( LicenseMap.UNKNOWN_LICENSE_MESSAGE );
            if ( !licenses.isEmpty() )
            {
                sb.append( "\n-------------------------------------------------------------------------------" );
                sb.append( "\n Already used licenses in project :" );
                for ( String license : licenses )
                {
                    sb.append( "\n - " ).append( license );
                }
            }
            sb.append( "\n-------------------------------------------------------------------------------" );
            sb.append( "\n Please fill the missing licenses for dependencies :\n\n" );


            if (getOverrideMappings() != null)
            {
                getOverrideMappings().store( writer, sb.toString() );
            }
        }
        finally
        {
            writer.close();
        }
    }
}
