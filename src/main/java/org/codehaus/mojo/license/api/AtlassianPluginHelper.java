package org.codehaus.mojo.license.api;


import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;

import java.util.List;
import java.util.Set;

/**
 * A helper component that resolves nested artifacts within Atlassian artifacts recursively. This is to discover
 * artifacts that have been nested by way of dependency:copy/copy-dependencies, the assembly plugin, or otherwise
 * that do not normally turn up using Maven's standard dependency resolution mechanisms.
 */
public interface AtlassianPluginHelper
{
    /**
     * Given a set of artifacts, will look for nested artifacts recursively and resolve them.
     *
     *
     * @param artifacts the starting set of artifacts from which to start searching for nested artifacts
     *                  to resolve.
     * @param remoteRepositories remote repositories to look up, usually injected via the Mojo paramter
     *                           <tt>project.remoteArtifactRepositories</tt>
     * @param localRepository local repositories to look up, usually injected via the Mojo paramter <tt>localRepository</tt>
     * @param verbose
     * @return The original set of artifacts, plus additional artifacts arising from the recursive resolution
     *         of nested artifacts.
     */
    Set<Artifact> resolveNestedArtifacts(Set<Artifact> artifacts,
                                         List<ArtifactRepository> remoteRepositories,
                                         ArtifactRepository localRepository, boolean verbose);

    boolean isAtlassianArtifact(Artifact artifact);
}
