package org.codehaus.mojo.license.api;

import org.codehaus.mojo.license.model.CandidateArtifact;

import java.util.Map;

class NestedArchiveSearchResult
{

    private final String path;
    private final Map<String, CandidateArtifact> fileToArtifact;

    public NestedArchiveSearchResult(final String path, final Map<String, CandidateArtifact> fileToArtifact)
    {
        this.path = path;
        this.fileToArtifact = fileToArtifact;
    }

    public String getPath()
    {
        return path;
    }

    public Map<String, CandidateArtifact> getFileToArtifact()
    {
        return fileToArtifact;
    }
}
