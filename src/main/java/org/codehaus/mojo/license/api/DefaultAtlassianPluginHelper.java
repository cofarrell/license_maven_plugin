package org.codehaus.mojo.license.api;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Maps;
import de.schlichtherle.truezip.file.TFile;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.AbstractArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.codehaus.mojo.license.model.CandidateArtifact;
import org.codehaus.mojo.license.utils.ArtifactCreator;
import org.codehaus.plexus.logging.AbstractLogEnabled;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Default implementation of the {@link AtlassianPluginHelper}.
 *
 * @plexus.component role="org.codehaus.mojo.license.api.AtlassianPluginHelper" role-hint="default"
 * @since 1.3-atlassian-1.4
 */
public class DefaultAtlassianPluginHelper extends AbstractLogEnabled
        implements AtlassianPluginHelper
{

    private static final int THREAD_POOL_SIZE = 4;
    /**
     * @plexus.requirement
     */
    private ArtifactResolver resolver;

    /**
     * @plexus.requirement
     */
    private ArtifactCreator artifactCreator;

    // Possibly use Guava cache if we run into memory problems
    private final Map<String, Map<String, CandidateArtifact>> pathToCandidateArtifactCache = Maps.newConcurrentMap();

    // Executor for searching nested archives
    private final ExecutorService nestedArchiveExecutor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    // Executor for converting candidate artifacts to artifacts
    private final ExecutorService artifactResolutionExecutor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    private final Supplier<ConcurrentMap<ArtifactResolutionRequest, List<Artifact>>> resolutionCache = Suppliers.memoize(new Supplier<ConcurrentMap<ArtifactResolutionRequest, List<Artifact>>>()
    {
        @Override
        public ConcurrentMap<ArtifactResolutionRequest, List<Artifact>> get()
        {
            return new MapMaker()
                    .makeComputingMap(new ArtifactResolutionFunction(resolver, artifactCreator, getLogger()));
        }
    });

    private static final String ATLASSIAN_GROUPID_PREFIX = "com.atlassian";

    DefaultAtlassianPluginHelper()
    {

    }

    DefaultAtlassianPluginHelper(final ArtifactResolver resolver,
                                 final ArtifactCreator artifactCreator)
    {
        this.resolver = resolver;
        this.artifactCreator = artifactCreator;
    }

    /**
     * @see AtlassianPluginHelper#resolveNestedArtifacts
     */
    public Set<Artifact> resolveNestedArtifacts(final Set<Artifact> artifacts,
                                                final List<ArtifactRepository> remoteRepositories,
                                                final ArtifactRepository localRepository, boolean verbose)
    {

        final Set<Artifact> collectedArtifacts = new HashSet<Artifact>(artifacts);
        // Don't swallow any exceptions when resolving nested artifacts. If an exception happens when
        // resolving dependencies, it's probably something we should find out, and resolution should
        // not continue.
        try
        {

            // Map from archive filename -> candidate artifact
            final Map<String, CandidateArtifact> fileToArtifact = getCandidateArtifactsNestedInJars(artifacts, remoteRepositories, localRepository);

            final CompletionService<List<Artifact>> artifactCompletionService = CompletionService.with(artifactResolutionExecutor);
            for (final Map.Entry<String, CandidateArtifact> fileToArtifactEntry : fileToArtifact.entrySet())
            {
                final CandidateArtifact candidateArtifact = fileToArtifactEntry.getValue();
                getLogger().debug("File - " + fileToArtifactEntry.getKey() + " contains " + candidateArtifact.toString());

                final ArtifactResolutionTask artifactResolutionTask = new ArtifactResolutionTask(candidateArtifact,
                        remoteRepositories, localRepository, resolutionCache.get(), verbose);
                artifactCompletionService.submit(artifactResolutionTask);
            }

            final List<List<Artifact>> resolutionResults = artifactCompletionService.get();
            for (final List<Artifact> resolutionResult : resolutionResults)
            {
                collectedArtifacts.addAll(resolutionResult);
            }
        }
        catch (Exception e)
        {
            // An exception has occurred when obtaining an artifact resolution from within an archive. We could
            // either die or continue, but I've chosen to die here.
            throw new RuntimeException(e);
        }
        return collectedArtifacts;
    }

    public boolean isAtlassianArtifact(final Artifact artifact)
    {
        return artifact != null && artifact.getGroupId() != null &&
                artifact.getGroupId().contains(ATLASSIAN_GROUPID_PREFIX);
    }

    // The private visibility of this method is a smell and should be fixed in future. Ideally, refactor this into a separate
    // component so it can be mocked and injected. Tests for this class currently only supply a jar and assume that
    // the component "does the right thing"
    private Map<String, CandidateArtifact> getCandidateArtifactsNestedInJars(final Set<Artifact> artifacts, List<ArtifactRepository> remoteRepositories, ArtifactRepository localRepository) throws ArtifactResolutionException, ArtifactNotFoundException
    {
        final Map<String, CandidateArtifact> allFileToArtifacts = new HashMap<String, CandidateArtifact>();

        final CompletionService<NestedArchiveSearchResult> nestedArchiveCompletionService = CompletionService.with(nestedArchiveExecutor);

        for (final Artifact artifact : artifacts)
        {
            if (isAtlassianArtifact(artifact))
            {
                if (artifact.getFile() == null)
                {
                    try
                    {
                        resolver.resolve(artifact, remoteRepositories, localRepository);
                        getLogger().debug("Resolving " + artifact.getId());
                    }
                    catch (AbstractArtifactResolutionException e)
                    {
                        getLogger().error("\tUnable to resolve dependency" + artifact.getId() + " (looking for nested jars)");
                    }
                }

                final TFile tFile = new TFile(artifact.getFile());
                final String absolutePath = tFile.getAbsolutePath();

                getLogger().debug("Doing nested jar resolution for " + absolutePath);
                final Map<String, CandidateArtifact> fileToCandidateArtifact = pathToCandidateArtifactCache.get(absolutePath);

                if (fileToCandidateArtifact != null)
                {
                    getLogger().debug(" Already found " + absolutePath + " in cache");
                    allFileToArtifacts.putAll(fileToCandidateArtifact);
                }
                else
                {
                    final Map<String, CandidateArtifact> fileToArtifact = new HashMap<String, CandidateArtifact>();
                    final NestedArchiveSearchTask task = new NestedArchiveSearchTask(artifact.getArtifactId(),
                            tFile, fileToArtifact, artifactCreator, getLogger());
                    nestedArchiveCompletionService.submit(task);
                }
            }
        }

        try
        {
            final List<NestedArchiveSearchResult> searchResults = nestedArchiveCompletionService.get();
            for (final NestedArchiveSearchResult searchResult : searchResults)
            {
                final Map<String, CandidateArtifact> fileToArtifact = searchResult.getFileToArtifact();
                allFileToArtifacts.putAll(fileToArtifact);
                pathToCandidateArtifactCache.put(searchResult.getPath(), fileToArtifact);

            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return allFileToArtifacts;
    }
}