package org.codehaus.mojo.license.api;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.License;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.codehaus.mojo.license.model.Dependency;
import org.codehaus.mojo.license.model.LicenseMap;
import org.codehaus.mojo.license.model.NonMavenDependency;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.mojo.license.utils.FileUtil;
import org.codehaus.mojo.license.utils.MojoHelper;
import org.codehaus.mojo.license.utils.SortedProperties;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.logging.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Default implementation of the third party tool.
 *
 * @author <a href="mailto:tchemit@codelutin.com">Tony Chemit</a>
 * @version $Id: DefaultThirdPartyTool.java 16640 2012-05-17 22:44:37Z tchemit $
 * @plexus.component role="org.codehaus.mojo.license.api.ThirdPartyTool" role-hint="default"
 */
public class DefaultThirdPartyTool
    extends AbstractLogEnabled
    implements ThirdPartyTool
{
    /**
     * Classifier of the third-parties descriptor attached to a maven module.
     */
    public static final String DESCRIPTOR_CLASSIFIER = "third-party";

    /**
     * Type of the the third-parties descriptor attached to a maven module.
     */
    public static final String DESCRIPTOR_TYPE = "properties";

    /**
     * Pattern of a GAV plus a type.
     */
    private static final Pattern GAV_PLUS_TYPE_PATTERN = Pattern.compile( "(.+)--(.+)--(.+)--(.+)" );

    /**
     * Pattenr of a GAV plus a type plus a classifier.
     */
    private static final Pattern GAV_PLUS_TYPE_AND_CLASSIFIER_PATTERN =
        Pattern.compile( "(.+)--(.+)--(.+)--(.+)--(.+)" );

    // ----------------------------------------------------------------------
    // Components
    // ----------------------------------------------------------------------

    /**
     * The component that is used to resolve additional artifacts required.
     *
     * @plexus.requirement
     */
    private ArtifactResolver artifactResolver;

    /**
     * The component used for creating artifact instances.
     *
     * @plexus.requirement
     */
    private ArtifactFactory artifactFactory;

    /**
     * Maven ProjectHelper.
     *
     * @plexus.requirement
     */
    private MavenProjectHelper projectHelper;

    /**
     * @plexus.requirement
     */
    private FreeMarkerHelper freeMarkerHelper;

    /**
     * Maven project comparator.
     */
    private final Comparator<MavenProject> mavenProjectComparator = MojoHelper.newMavenProjectComparator();

    /**
     * {@inheritDoc}
     */
    public void attachThirdPartyDescriptor( MavenProject project, File file )
    {
        projectHelper.attachArtifact( project, DESCRIPTOR_TYPE, DESCRIPTOR_CLASSIFIER, file );
    }

    /**
     * {@inheritDoc}
     */
    public SortedSet<MavenProject> getAllProjects( LicenseMap licenseMap, boolean doLog )
    {
        return licenseMap.getMavenProjects();
    }

    /**
     * {@inheritDoc}
     */
    public SortedSet<MavenProject> getMavenProjectsWithNoLicense( LicenseMap licenseMap, boolean doLog )
    {
        return licenseMap.getMavenProjectsByLicense(LicenseMap.UNKNOWN_LICENSE_MESSAGE);
    }

    /**
     * {@inheritDoc}
     */
    public SortedSet<Dependency> getProjectsWithNoLicense( LicenseMap licenseMap, boolean doLog )
    {

        Logger log = getLogger();

        // get unsafe dependencies (says with no license)
        SortedSet<Dependency> unsafeDependencies = licenseMap.getDependenciesByLicense(LicenseMap.UNKNOWN_LICENSE_MESSAGE);

        if ( doLog )
        {
            if ( CollectionUtils.isEmpty( unsafeDependencies ) )
            {
                log.debug( "There is no dependency with no license from poms." );
            }
            else
            {
                log.debug( "There is " + unsafeDependencies.size() + " dependencies with no license from poms : " );
                for ( Dependency dep : unsafeDependencies )
                {

                    // no license found for the dependency
                    log.debug( " - " + dep.getArtifactName() );
                }
            }
        }

        return unsafeDependencies != null? unsafeDependencies : new TreeSet<Dependency>();
    }



    /**
     * {@inheritDoc}
     */
    public SortedProperties resolveOverrideFilesFromRepository(String encoding,
                                                               Collection<MavenProject> projects,
                                                               ArtifactRepository localRepository,
                                                               List<ArtifactRepository> remoteRepositories)
        throws ThirdPartyToolException, IOException
    {

        SortedProperties result = new SortedProperties( encoding );

        for ( MavenProject mavenProject : projects )
        {

            File thirdPartyDescriptor = resolvThirdPartyDescriptor( mavenProject, localRepository, remoteRepositories );

            if ( thirdPartyDescriptor != null && thirdPartyDescriptor.exists() && thirdPartyDescriptor.length() > 0 )
            {

                if ( getLogger().isInfoEnabled() )
                {
                    getLogger().info( "Detects third party descriptor " + thirdPartyDescriptor );
                }

                // there is a third party file detected form the given dependency
                SortedProperties overrideMappings = new SortedProperties( encoding );

                if ( thirdPartyDescriptor.exists() )
                {

                    getLogger().info( "Load missing-license file " + thirdPartyDescriptor );

                    // load the missing file
                    overrideMappings.load(thirdPartyDescriptor);
                }

            }
        }
        return result;
    }

    public Set<NonMavenDependency> createNonMavenDependencies(File nonMavenDependenciesFile, String encoding, MavenProject mavenProject) throws IOException
    {
        Map<String, NonMavenDependency> nonMavenDependencies = new HashMap<String, NonMavenDependency>();
        if (nonMavenDependenciesFile.exists()){

            SortedProperties nonMavenDepFile = new SortedProperties( encoding );
            nonMavenDepFile.load( nonMavenDependenciesFile );

            for (Enumeration e = nonMavenDepFile.keys() ; e.hasMoreElements() ;) {
                String key = (String) e.nextElement();
                String type = key.substring(key.lastIndexOf(".") + 1, key.length());
                String id = key.substring(0, key.lastIndexOf("."));

                NonMavenDependency dependency = nonMavenDependencies.get(id);

                if ( dependency == null){
                    String[] idSplit = id.split("--");
                    dependency = new NonMavenDependency(idSplit[0], idSplit[1]);
                    nonMavenDependencies.put(id, dependency);

                }

                if (type.equals("license")){
                    dependency.setLicense((String) nonMavenDepFile.get(key));
                } else if (type.equals("url")){
                    dependency.setURL((String) nonMavenDepFile.get(key));
                } else if (type.equals("scope")){
                    final Scope scope = Scope.valueOfIgnoreCaseRestricted((String) nonMavenDepFile.get(key));
                    dependency.addScopeOnModule(mavenProject, scope);
                }

            }
        }
        TreeSet<NonMavenDependency> nonMavenSet = new TreeSet<NonMavenDependency>();
        nonMavenSet.addAll(nonMavenDependencies.values());
        return nonMavenSet;
    }

    /**
     * {@inheritDoc}
     */
    public File resolvThirdPartyDescriptor( MavenProject project, ArtifactRepository localRepository,
                                            List<ArtifactRepository> repositories )
        throws ThirdPartyToolException
    {
        if ( project == null )
        {
            throw new IllegalArgumentException( "The parameter 'project' can not be null" );
        }
        if ( localRepository == null )
        {
            throw new IllegalArgumentException( "The parameter 'localRepository' can not be null" );
        }
        if ( repositories == null )
        {
            throw new IllegalArgumentException( "The parameter 'remoteArtifactRepositories' can not be null" );
        }

        try
        {
            return resolveThirdPartyDescriptor( project, localRepository, repositories );
        }
        catch ( ArtifactNotFoundException e )
        {
            getLogger().debug( "ArtifactNotFoundException: Unable to locate third party descriptor: " + e );
            return null;
        }
        catch ( ArtifactResolutionException e )
        {
            throw new ThirdPartyToolException(
                "ArtifactResolutionException: Unable to locate third party descriptor: " + e.getMessage(), e );
        }
        catch ( IOException e )
        {
            throw new ThirdPartyToolException(
                "IOException: Unable to locate third party descriptor: " + e.getMessage(), e );
        }
    }




    public void overrideLicense(LicenseMap licenseMap, Dependency dependency, String license)
    {
        licenseMap.overrideLicenseAndScope(dependency, license);
    }


    public void addLicense(LicenseMap licenseMap, Dependency dependency, String licenseName)
    {
        License license = new License();
        license.setName(licenseName);
        license.setUrl(licenseName);

        addLicense(licenseMap, dependency, Arrays.asList(license));
    }


    /**
     * {@inheritDoc}
     */
    public void addLicense( LicenseMap licenseMap, Dependency dependency, List<?> licenses)
    {


        if ( Artifact.SCOPE_SYSTEM.equals( dependency.getArtifactScope() ) )
        {
            // do NOT treate system dependency
            return;
        }

        if ( CollectionUtils.isEmpty( licenses ) )
        {

            // no license found for the dependency
            licenseMap.addDependency(LicenseMap.UNKNOWN_LICENSE_MESSAGE, dependency);
            return;
        }

        for ( Object o : licenses )
        {
            String id = dependency.getId();
            if ( o == null )
            {
                getLogger().warn( "could not acquire the license for " + id );
                continue;
            }
            License license = (License) o;
            String licenseKey = license.getName();

            if ( !dependency.isComplete() )
            {
                String message = "Failed to include dependency \"" + id + "\"\nYou need to include a license and the license scope (" + Scope.getRestrictedValues() + "). ";

                getLogger().error("\n\n" + message + "\n\n");
                throw new IllegalArgumentException(message);
            }

            // tchemit 2010-08-29 Ano #816 Check if the License object is well formed

            if ( StringUtils.isEmpty( license.getName() ) )
            {
                getLogger().debug( "No license name defined for " + id );
                licenseKey = license.getUrl();
            }

            if ( StringUtils.isEmpty( licenseKey ) )
            {
                getLogger().debug( "No license url defined for " + id );
                licenseKey = LicenseMap.UNKNOWN_LICENSE_MESSAGE;
            }



            licenseMap.addDependency(licenseKey, dependency);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void mergeLicenses( LicenseMap licenseMap, String... licenses )
    {
        if ( licenses.length == 0 )
        {
            return;
        }

        String mainLicense = licenses[0].trim();
        SortedSet<Dependency> mainSet = licenseMap.getDependenciesByLicense(mainLicense);
        if ( mainSet == null )
        {
            getLogger().debug( "No license [" + mainLicense + "] found, will create it." );
            mainSet = new TreeSet<Dependency>();

        }
        int size = licenses.length;
        for ( int i = 1; i < size; i++ )
        {
            String license = licenses[i].trim();
            SortedSet<Dependency> set = licenseMap.getDependenciesByLicense(license);
            if ( set == null )
            {
                getLogger().debug( "No license [" + license + "] found, skip this merge." );
                continue;
            }
            getLogger().debug( "Merge license [" + license + "] (" + set.size() + " dependencies)." );

            mainSet.addAll( set );

            if (!mainSet.isEmpty())
            {
                licenseMap.addDependencies(mainLicense, mainSet);
            }

            set.clear();
            licenseMap.removeLicense(license);
        }
    }


    /**
     * {@inheritDoc}
     */
    public SortedProperties loadOverrideFile(LicenseMap licenseMap, Scope scope,
                                             Map<String, Dependency> artifactCache,
                                             String encoding, File missingFile, MavenProject project,
                                             boolean overrideAlways)
        throws IOException
    {

        SortedProperties unsafeMappings = new SortedProperties( encoding );

        if ( missingFile.exists() )
        {
            // there is some unsafe dependencies

            getLogger().info( "Load missing-license file " + missingFile );

            // load the missing file
            unsafeMappings.load( missingFile );
        }

        // get from the missing file, all unknown dependencies
        List<String> unknownDependenciesId = new ArrayList<String>();

        // coming from maven-licen-plugin, we used in id type and classifier, now we remove
        // these informations since GAV is good enough to qualify a license of any artifact of it...
        Map<String, String> migrateKeys = migrateMissingFileKeys( unsafeMappings.keySet() );

        for ( String id : migrateKeys.keySet() )
        {
            String migratedId = migrateKeys.get( id );

            Dependency dependency = artifactCache.get( migratedId );
            if ( dependency == null )
            {
                // now we are sure this is a unknown dependency
                unknownDependenciesId.add( id );
            }
            else
            {
                if ( !id.equals( migratedId ) )
                {

                    // migrates id to migratedId
                    getLogger().info( "Migrates [" + id + "] to [" + migratedId + "] in the missing file." );
                    Object value = unsafeMappings.get( id );
                    unsafeMappings.remove( id );
                    unsafeMappings.put( migratedId, value );
                }
            }
        }

        if ( !unknownDependenciesId.isEmpty() )
        {

            StringBuilder warnMsg = new StringBuilder("\n\n\nThe following entries in the input file can be deleted: \n");
            for ( String id : unknownDependenciesId )
            {
               warnMsg.append(" -  ").append(id).append("\n");
            }
            getLogger().warn(warnMsg.toString());
        }


        // push back loaded dependencies
        for ( Object o : unsafeMappings.keySet() )
        {
            String id = (String) o;

            Dependency dependency = artifactCache.get( id );
            if ( dependency == null )
            {

                getLogger().debug( "dependency [" + id + "] does not exist in project. " );
                continue;
            }

            String license = (String) unsafeMappings.get( id );
            if ( StringUtils.isEmpty( license ) )
            {
                // empty license means not fill, skip it
                continue;
            }

            // you should override the license ONLY if the dependency is on that module
            // and the license is the same. I.E -> a test override can't override a binary
            if (overrideAlways
                    || (dependency.containsModule(project) && dependency.hasLowerEqualsScope(scope)) )
            {
                overrideLicense(licenseMap, dependency, license);
            }
            else
            {
                getLogger().warn("Override to artifact " + id + " in module " + project.getArtifact().getId() + " won't be used");
            }


        }

        return unsafeMappings;
    }

    /**
     * {@inheritDoc}
     */
    public void writeThirdPartyFile( LicenseMap licenseMap, File thirdPartyFile, boolean verbose, String encoding,
                                     String lineFormat, boolean exportToCSV)
        throws IOException
    {

        Logger log = getLogger();
        String content = "";

        if (exportToCSV){

            TreeSet<String> csvLines = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);


            Map<Dependency,String[]> dependencyMap = licenseMap.getLicensesPerDependency();


            for (Dependency project : dependencyMap.keySet()){
                StringBuilder line = new StringBuilder();
                line.append(project.getName().replaceAll(",", " - ")).append(",");
                line.append(project.getGAVInfo()).append(",");
                String[] licenses = dependencyMap.get(project);
                line.append(StringUtils.join(licenses, ";").replaceAll("\n", "").replaceAll(",", " - "));
                line.append(",");
                if (project.getUrl() != null)
                {
                    line.append(project.getUrl());
                }

                line.append(",");
                line.append(project.getLicenseScope().getPrintableName());

                csvLines.add(line.toString());
            }

            StringBuilder csv = new StringBuilder("# ######################################\n");
            csv.append("# This file is automatically generated by the license maven plugin. \n");
            csv.append("# Do not edit it by hand. \n");
            csv.append("# ######################################\n");
            csv.append("Component, GAV, License, URL, Scope\n");


            for (String line : csvLines){
                csv.append(line).append("\n");
            }


            content = csv.toString();


        } else {
            Map<String, Object> properties = new HashMap<String, Object>();
            properties.put( "licenseMap", licenseMap.getMavenProjectsPerLicense().entrySet()   );
            properties.put( "dependencyMap", licenseMap.getLicensesPerMavenProject().entrySet() );
            content = freeMarkerHelper.renderTemplate( lineFormat, properties );
        }


        log.info( "Writing third-party file to " + thirdPartyFile );

        FileUtil.writeString( thirdPartyFile, content, encoding );

    }

    /**
     * {@inheritDoc}
     */
    public void writeBundleThirdPartyFile( File thirdPartyFile, File outputDirectory, String bundleThirdPartyPath )
        throws IOException
    {

        // creates the bundled license file
        File bundleTarget = FileUtil.getFile( outputDirectory, bundleThirdPartyPath );
        getLogger().info( "Writing bundled third-party file to " + bundleTarget );
        FileUtil.copyFile( thirdPartyFile, bundleTarget );
    }

    // ----------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------

    /**
     *
     * @param project         not null
     * @param localRepository not null
     * @param repositories    not null
     * @return the resolved site descriptor
     * @throws IOException                 if any
     * @throws ArtifactResolutionException if any
     * @throws ArtifactNotFoundException   if any
     */
    private File resolveThirdPartyDescriptor( MavenProject project, ArtifactRepository localRepository,
                                              List<ArtifactRepository> repositories )
        throws IOException, ArtifactResolutionException, ArtifactNotFoundException
    {
        File result;

        // TODO: this is a bit crude - proper type, or proper handling as metadata rather than an artifact in 2.1?
        Artifact artifact = artifactFactory.createArtifactWithClassifier( project.getGroupId(), project.getArtifactId(),
                                                                          project.getVersion(), DESCRIPTOR_TYPE,
                                                                          DESCRIPTOR_CLASSIFIER );
        try
        {
            artifactResolver.resolve( artifact, repositories, localRepository );

            result = artifact.getFile();

            // we use zero length files to avoid re-resolution (see below)
            if ( result.length() == 0 )
            {
                getLogger().debug( "Skipped third party descriptor" );
            }
        }
        catch ( ArtifactNotFoundException e )
        {
            getLogger().debug( "Unable to locate third party files descriptor : " + e );

            // we can afford to write an empty descriptor here as we don't expect it to turn up later in the remote
            // repository, because the parent was already released (and snapshots are updated automatically if changed)
            result = new File( localRepository.getBasedir(), localRepository.pathOf( artifact ) );

            FileUtil.createNewFile( result );
        }

        return result;
    }

    private Map<String, String> migrateMissingFileKeys( Set<Object> missingFileKeys )
    {
        Map<String, String> migrateKeys = new HashMap<String, String>();
        for ( Object object : missingFileKeys )
        {
            String id = (String) object;
            Matcher matcher;

            String newId = id;
            matcher = GAV_PLUS_TYPE_AND_CLASSIFIER_PATTERN.matcher( id );
            if ( matcher.matches() )
            {
                newId = matcher.group( 1 ) + "--" + matcher.group( 2 ) + "--" + matcher.group( 3 );

            }
            else
            {
                matcher = GAV_PLUS_TYPE_PATTERN.matcher( id );
                if ( matcher.matches() )
                {
                    newId = matcher.group( 1 ) + "--" + matcher.group( 2 ) + "--" + matcher.group( 3 );

                }
            }
            migrateKeys.put( id, newId );
        }
        return migrateKeys;
    }


}
