package org.codehaus.mojo.license.api;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public class CompletionService<T>
{
    private final ExecutorCompletionService<T> executorCompletionService;
    private final AtomicInteger tasks = new AtomicInteger(0);

    private CompletionService(final ExecutorService executorService)
    {
        this.executorCompletionService = new ExecutorCompletionService<T>(executorService);
    }

    public static <E> CompletionService<E> with(final ExecutorService executorService)
    {
        return new CompletionService<E>(executorService);
    }

    public void submit(Callable<T> task)
    {
        tasks.incrementAndGet();
        executorCompletionService.submit(task);
    }

    public List<T> get() throws InterruptedException, ExecutionException
    {
        final List<T> results = Lists.newArrayList();
        for (int i = 0; i < tasks.get(); ++i)
        {
            results.add(executorCompletionService.take().get());
        }
        return results;
    }
}
