package org.codehaus.mojo.license.api;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.MapMaker;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactCollector;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.shared.dependency.tree.DependencyNode;
import org.apache.maven.shared.dependency.tree.DependencyTreeBuilder;
import org.apache.maven.shared.dependency.tree.DependencyTreeBuilderException;
import org.codehaus.mojo.license.model.Dependency;
import org.codehaus.mojo.license.model.MavenDependency;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.mojo.license.utils.MojoHelper;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.logging.Logger;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Default implementation of the {@link DependenciesTool}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @version $Id: DefaultDependenciesTool.java 16640 2012-05-17 22:44:37Z tchemit $
 * @plexus.component role="org.codehaus.mojo.license.api.DependenciesTool" role-hint="default"
 * @since 1.0
 */
public class DefaultDependenciesTool
    extends AbstractLogEnabled
    implements DependenciesTool
{

    /**
     * Message used when an invalid expression pattern is found.
     */
    public static final String INVALID_PATTERN_MESSAGE =
        "The pattern specified by expression <%s> seems to be invalid.";

    /**
     * Project builder.
     *
     * @plexus.requirement
     */
    private MavenProjectBuilder mavenProjectBuilder;

    /**
     * Helper to resolve nested artifacts in Atlassian artifacts
     *
     * @plexus.requirement
     */
    private AtlassianPluginHelper atlassianPluginHelper;


    /**
     * @plexus.requirement
     */
    private ArtifactMetadataSource artifactMetadataSource;

    /**
     * @plexus.requirement
     */
    private ArtifactCollector artifactCollector;

    /**
     * @plexus.requirement
     */
    private DependencyTreeBuilder treeBuilder;

    /**
     * @plexus.requirement
     */
    private ArtifactFactory artifactFactory;

    private ExecutorService dependencyResolutionExecutor = Executors.newFixedThreadPool(4);

    private final ConcurrentMap<ArtifactRequest, MavenProject> artifactCache = new MapMaker().
            makeComputingMap(new Function<ArtifactRequest, MavenProject>()
            {
                @Override
                public MavenProject apply(final ArtifactRequest input)
                {
                    try
                    {
                        return mavenProjectBuilder.buildFromRepository(input.getArtifact(),
                                input.getRemoteRepositories(), input.getLocalRepository(), true);
                    }
                    catch (ProjectBuildingException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
            });

    /**
     * {@inheritDoc}
     */
    public SortedMap<String, Dependency> loadProjectDependencies(MavenProject project,
                                                                 MavenProjectDependenciesConfigurator configuration,
                                                                 ArtifactRepository localRepository,
                                                                 List<ArtifactRepository> remoteRepositories,
                                                                 Scope moduleScope, Set<String> ignoredTrees)
    {

        if (moduleScope == Scope.IGNORE)
            return new TreeMap<String, Dependency>();


        SortedMap<String,MavenDependency> mavenCache = new ConcurrentSkipListMap<String, MavenDependency>();


        boolean haveNoIncludedGroups = StringUtils.isEmpty( configuration.getIncludedGroups() );
        boolean haveNoIncludedArtifacts = StringUtils.isEmpty( configuration.getIncludedArtifacts() );

        boolean haveExcludedGroups = StringUtils.isNotEmpty( configuration.getExcludedGroups() );
        boolean haveExcludedArtifacts = StringUtils.isNotEmpty( configuration.getExcludedArtifacts() );
        boolean haveExclusions = haveExcludedGroups || haveExcludedArtifacts;

        Pattern includedGroupPattern = null;
        Pattern includedArtifactPattern = null;
        Pattern excludedGroupPattern = null;
        Pattern excludedArtifactPattern = null;


        if ( !haveNoIncludedGroups )
        {
            includedGroupPattern = Pattern.compile( configuration.getIncludedGroups() );
        }
        if ( !haveNoIncludedArtifacts )
        {
            includedArtifactPattern = Pattern.compile( configuration.getIncludedArtifacts() );
        }
        if ( haveExcludedGroups )
        {
            excludedGroupPattern = Pattern.compile( configuration.getExcludedGroups() );
        }
        if ( haveExcludedArtifacts )
        {
            excludedArtifactPattern = Pattern.compile( configuration.getExcludedArtifacts() );
        }

        Set<Artifact> depArtifacts;

        if ( configuration.isIncludeTransitiveDependencies() )
        {
            if (ignoredTrees == null || ignoredTrees.isEmpty())
            {
                depArtifacts = project.getArtifacts();
            }
            else
            {
                depArtifacts = retrieveDependenciesFromTree(project, localRepository, ignoredTrees);
            }

        }
        else
        {
            // Only direct project dependencies
            depArtifacts = project.getDependencyArtifacts();
        }
        if ( configuration.isResolveNestedAtlassianArtifacts() )
        {
                depArtifacts = atlassianPluginHelper.resolveNestedArtifacts(depArtifacts,
                        remoteRepositories, localRepository, configuration.isVerbose());
        }

        List<String> includedScopes = configuration.getIncludedScopes();
        List<String> excludeScopes = configuration.getExcludedScopes();

        boolean verbose = configuration.isVerbose();

        SortedMap<String, Dependency> result = new TreeMap<String, Dependency>();

        final CompletionService<ResolveTaskResult> dependencyResolutionService = CompletionService.with(dependencyResolutionExecutor);
        for (final Artifact artifact : depArtifacts )
        {

            final DependencyResolutionTask dependencyResolutionTask = new DependencyResolutionTask(
                    project,
                    localRepository,
                    remoteRepositories,
                    moduleScope,
                    mavenCache,
                    haveNoIncludedGroups,
                    haveNoIncludedArtifacts,
                    haveExclusions,
                    includedGroupPattern,
                    includedArtifactPattern,
                    excludedGroupPattern,
                    excludedArtifactPattern,
                    includedScopes,
                    excludeScopes,
                    verbose,
                    artifact,
                    artifactCache,
                    mavenProjectBuilder,
                    getLogger());
            dependencyResolutionService.submit(dependencyResolutionTask);
        }

        try
        {
            final List<ResolveTaskResult> resolveTaskResults = dependencyResolutionService.get();
            for ( final ResolveTaskResult resolveTaskResult : resolveTaskResults )
            {
                if (resolveTaskResult != null)
                {
                    result.put(resolveTaskResult.getArtifactId(), resolveTaskResult.getDepMavenProject());
                }
            }
        }
        catch (Exception e)
        {
            getLogger().error("An error occurred when resolving dependent artifacts for " + project.getArtifactId(), e);
        }

        return result;
    }

    private static class ResolveTaskResult
    {

        private final String artifactId;
        private final MavenDependency depMavenProject;

        public ResolveTaskResult(String artifactId, MavenDependency depMavenProject)
        {
            this.artifactId = artifactId;
            this.depMavenProject = depMavenProject;
        }

        public String getArtifactId()
        {
            return artifactId;
        }

        public MavenDependency getDepMavenProject()
        {
            return depMavenProject;
        }
    }

    private static class DependencyResolutionTask implements Callable<ResolveTaskResult>
    {
        private final MavenProject project;
        private final ArtifactRepository localRepository;
        private final List<ArtifactRepository> remoteRepositories;
        private final Scope moduleScope;
        private final SortedMap<String, MavenDependency> mavenCache;
        private final boolean haveNoIncludedGroups;
        private final boolean haveNoIncludedArtifacts;
        private final boolean haveExclusions;
        private final Pattern includedGroupPattern;
        private final Pattern includedArtifactPattern;
        private final Pattern excludedGroupPattern;
        private final Pattern excludedArtifactPattern;
        private final List<String> includedScopes;
        private final List<String> excludeScopes;
        private final boolean verbose;
        private final Artifact artifact;
        private final MavenProjectBuilder mavenProjectBuilder;
        private final Logger logger;
        private final ConcurrentMap<ArtifactRequest, MavenProject> artifactCache;

        public DependencyResolutionTask(MavenProject project, ArtifactRepository localRepository,
                                        List<ArtifactRepository> remoteRepositories, Scope moduleScope,
                                        SortedMap<String, MavenDependency> mavenCache, boolean haveNoIncludedGroups,
                                        boolean haveNoIncludedArtifacts, boolean haveExclusions, Pattern includedGroupPattern,
                                        Pattern includedArtifactPattern, Pattern excludedGroupPattern, Pattern excludedArtifactPattern,
                                        List<String> includedScopes, List<String> excludeScopes, boolean verbose,
                                        Artifact artifact, ConcurrentMap<ArtifactRequest, MavenProject> artifactCache,
                                        MavenProjectBuilder mavenProjectBuilder, Logger logger)
        {

            this.project = project;

            this.localRepository = localRepository;
            this.remoteRepositories = remoteRepositories;
            this.moduleScope = moduleScope;
            this.mavenCache = mavenCache;
            this.haveNoIncludedGroups = haveNoIncludedGroups;
            this.haveNoIncludedArtifacts = haveNoIncludedArtifacts;
            this.haveExclusions = haveExclusions;
            this.includedGroupPattern = includedGroupPattern;
            this.includedArtifactPattern = includedArtifactPattern;
            this.excludedGroupPattern = excludedGroupPattern;
            this.excludedArtifactPattern = excludedArtifactPattern;
            this.includedScopes = includedScopes;
            this.excludeScopes = excludeScopes;
            this.verbose = verbose;
            this.artifact = artifact;
            this.mavenProjectBuilder = mavenProjectBuilder;
            this.logger = logger;
            this.artifactCache = artifactCache;
        }

        public ResolveTaskResult call()
        {
            String scope = artifact.getScope();
            if ( CollectionUtils.isNotEmpty(includedScopes) && !includedScopes.contains( scope ) )
            {
                // not in included scopes
                return null;
            }

            if ( excludeScopes.contains( scope ) )
            {
                // in exluced scopes
                return null;
            }

            String id = MojoHelper.getArtifactIdWithScope(artifact);

            if ( verbose )
            {
                logger.info( "Detected artifact [" + id  + "] - project " + project.getName());
            }

            // Check if the project should be included
            // If there is no specified artifacts and group to include, include all
            boolean isToInclude = haveNoIncludedArtifacts && haveNoIncludedGroups ||
                    isIncludable( artifact, includedGroupPattern, includedArtifactPattern );

            // Check if the project should be excluded
            boolean isToExclude = isToInclude && haveExclusions &&
                    isExcludable( artifact, excludedGroupPattern, excludedArtifactPattern );

            if ( !isToInclude || isToExclude )
            {
                if ( verbose )
                {
                    logger.info( "Skipped artifact [" + id + "]" );
                }
                return null;
            }

            MavenDependency depMavenProject = null;

            if ( mavenCache != null )
            {
                // try to get project from mavenCache
                depMavenProject = mavenCache.get( id );
            }


            if ( depMavenProject == null )
            {
                // build project
                MavenProject mavenProject = artifactCache.get(new ArtifactRequest(artifact, remoteRepositories, localRepository));

                mavenProject.getArtifact().setScope( artifact.getScope() );

                depMavenProject = new MavenDependency(project, mavenProject, moduleScope);

                if ( verbose )
                {
                    logger.info( "Added dependency [" + id + "]" );
                }
                if ( mavenCache != null )
                {

                    // store it also in mavenCache
                    mavenCache.put( id, depMavenProject );
                }
            }

            // keep the project
            return new ResolveTaskResult( MojoHelper.getArtifactId(artifact), depMavenProject );
        }

        /**
         * Tests if the given project is includable against a groupdId pattern and a artifact pattern.
         *
         * @param project                 the project to test
         * @param includedGroupPattern    the include group pattern
         * @param includedArtifactPattern the include artifact pattenr
         * @return {@code true} if the project is includavble, {@code false} otherwise
         */
        protected boolean isIncludable( Artifact project, Pattern includedGroupPattern, Pattern includedArtifactPattern )
        {

            // check if the groupId of the project should be included
            if ( includedGroupPattern != null )
            {
                // we have some defined license filters
                try
                {
                    Matcher matchGroupId = includedGroupPattern.matcher( project.getGroupId() );
                    if ( matchGroupId.find() )
                    {
                        if ( logger.isDebugEnabled() )
                        {
                            logger.debug( "Include " + project.getGroupId() );
                        }
                        return true;
                    }
                }
                catch ( PatternSyntaxException e )
                {
                    logger.warn( String.format( INVALID_PATTERN_MESSAGE, includedGroupPattern.pattern() ) );
                }
            }

            // check if the artifactId of the project should be included
            if ( includedArtifactPattern != null )
            {
                // we have some defined license filters
                try
                {
                    Matcher matchGroupId = includedArtifactPattern.matcher( project.getArtifactId() );
                    if ( matchGroupId.find() )
                    {
                        if ( logger.isDebugEnabled() )
                        {
                            logger.debug( "Include " + project.getArtifactId() );
                        }
                        return true;
                    }
                }
                catch ( PatternSyntaxException e )
                {
                    logger.warn( String.format( INVALID_PATTERN_MESSAGE, includedArtifactPattern.pattern() ) );
                }
            }
            return false;
        }

        /**
         * Tests if the given project is excludable against a groupdId pattern and a artifact pattern.
         *
         * @param project                 the project to test
         * @param excludedGroupPattern    the exlcude group pattern
         * @param excludedArtifactPattern the exclude artifact pattenr
         * @return {@code true} if the project is excludable, {@code false} otherwise
         */
        protected boolean isExcludable( Artifact project, Pattern excludedGroupPattern, Pattern excludedArtifactPattern )
        {

            // check if the groupId of the project should be included
            if ( excludedGroupPattern != null )
            {
                // we have some defined license filters
                try
                {
                    Matcher matchGroupId = excludedGroupPattern.matcher( project.getGroupId() );
                    if ( matchGroupId.find() )
                    {
                        if ( logger.isDebugEnabled() )
                        {
                            logger.debug( "Exclude " + project.getGroupId() );
                        }
                        return true;
                    }
                }
                catch ( PatternSyntaxException e )
                {
                    logger.warn( String.format( INVALID_PATTERN_MESSAGE, excludedGroupPattern.pattern() ) );
                }
            }

            // check if the artifactId of the project should be included
            if ( excludedArtifactPattern != null )
            {
                // we have some defined license filters
                try
                {
                    Matcher matchGroupId = excludedArtifactPattern.matcher( project.getArtifactId() );
                    if ( matchGroupId.find() )
                    {
                        if ( logger.isDebugEnabled() )
                        {
                            logger.debug( "Exclude " + project.getArtifactId() );
                        }
                        return true;
                    }
                }
                catch ( PatternSyntaxException e )
                {
                    logger.warn( String.format( INVALID_PATTERN_MESSAGE, excludedArtifactPattern.pattern() ) );
                }
            }
            return false;
        }


    }

    private Set<Artifact> retrieveDependenciesFromTree(final MavenProject project, final ArtifactRepository localRepository, final Set<String> ignoredTrees)
    {

        DependencyNode rootNode = null;
        try
        {
            rootNode = treeBuilder.buildDependencyTree(project, localRepository,
                    artifactFactory, artifactMetadataSource, null, artifactCollector);
        }
        catch (DependencyTreeBuilderException e)
        {
            throw new IllegalArgumentException("The project couldn't generate a dependency tree", e);
        }


        final Set<Artifact> depArtifacts = new HashSet<Artifact>();


        final Queue<DependencyNode> nodesToVisit = new LinkedList<DependencyNode>(rootNode.getChildren());
        DependencyNode actualNode = null;
        while ( (actualNode = nodesToVisit.poll()) != null)
        {
            if (!isNodeIdPresentOnSet(ignoredTrees, actualNode))
            {
                if (actualNode.getState() == DependencyNode.INCLUDED)
                {
                    depArtifacts.add(actualNode.getArtifact());
                }
                nodesToVisit.addAll(actualNode.getChildren());
            }
        }
        return depArtifacts;
    }


    private boolean isNodeIdPresentOnSet(Set<String> artifacts, DependencyNode node)
    {
        Artifact artifact = node.getArtifact();
        String prefix = artifact.getGroupId() + ":" + artifact.getArtifactId();
        return artifacts.contains(prefix);
    }

    private static class ArtifactRequest
    {
        private final Artifact artifact;
        private final List<ArtifactRepository> remoteRepositories;
        private final ArtifactRepository localRepository;

        public ArtifactRequest(final Artifact artifact, final List<ArtifactRepository> remoteRepositories,
                               final ArtifactRepository localRepository) {

            this.artifact = artifact;
            this.remoteRepositories = remoteRepositories;
            this.localRepository = localRepository;
        }

        public Artifact getArtifact()
        {
            return artifact;
        }

        public List<ArtifactRepository> getRemoteRepositories()
        {
            return remoteRepositories;
        }

        public ArtifactRepository getLocalRepository()
        {
            return localRepository;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ArtifactRequest that = (ArtifactRequest) o;

            if (artifact != null ? !artifact.equals(that.artifact) : that.artifact != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return artifact != null ? artifact.hashCode() : 0;
        }
    }
}
