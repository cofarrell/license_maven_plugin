package org.codehaus.mojo.license;


import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.mojo.license.model.Scope;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * This goal receives a BOM and assumes it's correct.
 */
@Mojo( name = "files-check", requiresProject = false, aggregator = true )
public class LicenseFilesCheckMojo extends AbstractLicenseMojo
{

    private static final String SEPARATOR = "--";
    private static final String FILEPATH_FORBIDDEN_CHARS = "[^A-Za-z0-9.-]";
    private static final String GAV_PATTERN = "([^:]+):([^:]+):[^:]+:([^:]+)";


    @Parameter( property = "license.licensesDirectory", defaultValue = "licenses", required = true )
    private File licensesDirectory;


    private List<String> unreviewedLicenseFiles;

    private File thirdPartyFile;



    @Override
    protected void init() throws Exception
    {

        thirdPartyFile = new File(getOutputDirectory(), getThirdPartyFilename());

        if (!thirdPartyFile.exists() || !thirdPartyFile.isFile())
        {
            throw new IllegalArgumentException("The BOM file "+ thirdPartyFile.getAbsolutePath() + " doesn't exist. ");
        }

        if (!licensesDirectory.exists() || !licensesDirectory.isDirectory())
        {
            throw new IllegalArgumentException("The license directory "+ licensesDirectory.getAbsolutePath() + " doesn't exist");
        }

        // files finishing with .txt on the license folder
        final File[] files = licensesDirectory.listFiles(new FileFilter()
        {
            public boolean accept(File pathname)
            {
                return pathname.isFile()
                        && pathname.getName().toLowerCase().endsWith(".txt");
            }
        });

        unreviewedLicenseFiles = new ArrayList<String>();
        for (File file : files)
        {
            unreviewedLicenseFiles.add(file.getName());
        }


    }

    @Override
    protected void doAction() throws Exception
    {
        final BufferedReader inputStream = new BufferedReader(new FileReader(thirdPartyFile));
        final Map<String, String> missingFiles = new HashMap<String, String>();

        String line = "";
        while ((line = inputStream.readLine()) != null )
        {
            if (line.startsWith("#")) //skip comments in the bom
            {
                continue;
            }

            // the BOM comes from the generate-bom goal
            final String[] columns = line.split(",");
            final String componentName = columns[0];
            final String groupArtifactidVersion = columns[1];
            final String license = columns[2];
            final String scope = columns[4];


            if (Scope.BINARY.getPrintableName().equalsIgnoreCase(scope)) // will skip the header as well
            {
                final String fileName = retrieveFilename(componentName, groupArtifactidVersion);

                if (!unreviewedLicenseFiles.contains(fileName))
                {
                    missingFiles.put(fileName, componentName + " | " + groupArtifactidVersion + " | " + license);
                    getLog().debug("Missing license file: " + fileName);

                }
                else
                {
                    unreviewedLicenseFiles.remove(fileName);
                    getLog().debug("Existing license file: " + fileName);
                }
            }

        }

        // Unused files message
        if (!unreviewedLicenseFiles.isEmpty())
        {
            StringBuilder unusedFilesMsg = new StringBuilder("\n\nThe following files are not necessary:\n");

            for (String filePath : unreviewedLicenseFiles)
            {
                unusedFilesMsg.append(" - ").append(filePath).append("\n");
            }
            unusedFilesMsg.append("\n\n");

            getLog().warn(unusedFilesMsg);
        }

        // Missing file errors
        if (!missingFiles.isEmpty())
        {
            final StringBuilder errorMsg = new StringBuilder("\n\nThe following files are missing in the license folder:\n");

            for (Map.Entry<String, String> entry : missingFiles.entrySet())
            {

                errorMsg.append(String.format(" - %s \n\t ( %s )\n", entry.getKey(), entry.getValue()));

                //Try fo give the user some hints of files to rename/update
                final String filePrefix = retrievePrefixFromFileName(entry.getKey());
                for (String unusedFile : unreviewedLicenseFiles)
                {
                    if (retrievePrefixFromFileName(unusedFile).equals(filePrefix))
                    {
                        errorMsg.append("\t Consider updating ").append(unusedFile).append("\n");
                    }
                }
            }
            errorMsg.append("\n\n");

            getLog().error(errorMsg);
            throw new FileNotFoundException("Some license files are missing. See message above for details. ");
        }



    }


    private String retrievePrefixFromFileName(final String fileName)
    {
        return fileName.substring(0, fileName.lastIndexOf(SEPARATOR));
    }

    //The rules for the file names can be found here: https://extranet.atlassian.com/x/S52Keg
    private String retrieveFilename(final String componentName, final String groupArtifactidVersion)
    {

        if (groupArtifactidVersion.matches(GAV_PATTERN))            // a maven artifact with a real groupArtifactidVersion
        {
            return groupArtifactidVersion.replaceAll(GAV_PATTERN, "$1" + SEPARATOR + "$2" + SEPARATOR + "$3.txt");
        }
        else                                                   // a non-maven artifact
        {
            return (componentName + SEPARATOR  + groupArtifactidVersion + ".txt").replaceAll(FILEPATH_FORBIDDEN_CHARS, "");
        }


    }
}
