package org.codehaus.mojo.license;


import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.mojo.license.model.License;
import org.codehaus.mojo.license.model.LicenseStore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

@Mojo( name = "license-files", requiresProject = false, aggregator = true )
public class LicensesTemplatesMojo extends AbstractLicenseMojo
{

    @Parameter( property = "license.thirdPartyFilename", defaultValue = "third-party-licensing/bom.csv", required = true )
    private File thirdPartyFilename;

    @Parameter( property = "license.generateTemplates", defaultValue = "false")
    private boolean generateTemplates;


    @Parameter( property = "license.licensesDirectory", defaultValue = "licenses", required = true )
    private File licensesDirectory;

    @Parameter( property = "license.excludedLicenses", defaultValue = "Atlassian 3.0 End User License Agreement", required = true )
    private String excludedLicenses;


    /**
     * Store of licenses.
     */
    private LicenseStore licenseStore;


    @Override
    protected void init() throws Exception
    {

        if (!thirdPartyFilename.exists() || !thirdPartyFilename.isFile())
        {
            throw new IllegalArgumentException("The file "+ thirdPartyFilename.getAbsolutePath() + "doesn't exist. ");
        }

        if (!licensesDirectory.exists() || !licensesDirectory.isDirectory())
        {
            throw new IllegalArgumentException("The directory "+ licensesDirectory.getAbsolutePath() + "doesn't exist");
        }

        licenseStore = LicenseStore.createLicenseStore( getLog(), null );
    }

    @Override
    protected void doAction() throws Exception
    {
        if (generateTemplates)
        {
            BufferedReader inputStream = new BufferedReader(new FileReader(thirdPartyFilename));


            String line;
            //the CSV header
            while ((line = inputStream.readLine()) != null )
            {
                if (!line.startsWith("#"))
                {
                    break;
                }
            }


            while ((line = inputStream.readLine()) != null )
            {
                if (line.startsWith("#"))
                {
                    continue;
                }

                String[] columns = line.split(",");
                String name = columns[0];
                String GAV = columns[1];
                String licenseName = columns[2];
                String scope = columns[4];


                if (scope.equalsIgnoreCase("binary"))
                {
                    String fileName = replaceName(name);

                    File licenseFile = new File(licensesDirectory, fileName);
                    if (!licenseFile.exists())
                    {
                        licenseFile.createNewFile();

                        getLog().debug("Creating license file: " + licenseFile.getAbsolutePath());
                        BufferedWriter fileContent = new BufferedWriter(new FileWriter(licenseFile));
                        fileContent.write(line);


                        License license = licenseStore.getLicenseByDescription(licenseName);
                        if (license != null)
                        {
                            fileContent.write("\n\n");
                            fileContent.write(license.getLicenseContent(getEncoding()));
                        }
                        else
                        {
                            getLog().warn("Could not create license from template: " + licenseFile.getAbsolutePath());
                        }

                        fileContent.close();
                    } else {
                        getLog().warn("Existing license file: " + licenseFile.getAbsolutePath());
                    }
                }

            }


        }
    }

    private String replaceName(String name)
    {

        return name.replaceAll("\\W", "").concat("_LICENSE.txt");
    }
}
