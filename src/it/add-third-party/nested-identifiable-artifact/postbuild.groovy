/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

file = new File(basedir, 'target/generated-sources/license/third.txt');
assert file.exists();
content = file.text;
assert !content.contains('the project has no dependencies.');
assert content.contains('(Unknown license) Atlassian Navigation Links API (com.atlassian.plugins:atlassian-nav-links-api:2.0.2 - http://www.atlassian.com/atlassian-closedsource-pom/atlassian-nav-links/atlassian-nav-links-api/)')
assert content.contains('(Unknown license) Atlassian Navigation Links Plugin (com.atlassian.plugins:atlassian-nav-links-plugin:2.0.2 - http://www.atlassian.com/atlassian-closedsource-pom/atlassian-nav-links/atlassian-nav-links-plugin/)')
assert content.contains('(Unknown license) atlassian-nav-links-spi (com.atlassian.plugins:atlassian-nav-links-spi:2.0.2 - http://www.atlassian.com/atlassian-closedsource-pom/atlassian-nav-links/atlassian-nav-links-spi/)')
assert content.contains('(Apache License) HttpClient (org.apache.httpcomponents:httpclient:4.1.2 - http://hc.apache.org/httpcomponents-client)')
assert content.contains('(Apache License) HttpClient Cache (org.apache.httpcomponents:httpclient-cache:4.1.2 - http://hc.apache.org/httpcomponents-client)')
assert content.contains('(Apache License) HttpCore (org.apache.httpcomponents:httpcore:4.1.2 - http://hc.apache.org/httpcomponents-core-ga)')

file = new File(basedir, 'target/generated-sources/license/test/third.txt');
assert file.exists();
content = file.text;
assert !content.contains('the project has no dependencies.');
assert content.contains('(Unknown license) Atlassian Navigation Links API (com.atlassian.plugins:atlassian-nav-links-api:2.0.2 - http://www.atlassian.com/atlassian-closedsource-pom/atlassian-nav-links/atlassian-nav-links-api/)')
assert content.contains('(Unknown license) Atlassian Navigation Links Plugin (com.atlassian.plugins:atlassian-nav-links-plugin:2.0.2 - http://www.atlassian.com/atlassian-closedsource-pom/atlassian-nav-links/atlassian-nav-links-plugin/)')
assert content.contains('(Unknown license) atlassian-nav-links-spi (com.atlassian.plugins:atlassian-nav-links-spi:2.0.2 - http://www.atlassian.com/atlassian-closedsource-pom/atlassian-nav-links/atlassian-nav-links-spi/)')
assert content.contains('(Apache License) HttpClient (org.apache.httpcomponents:httpclient:4.1.2 - http://hc.apache.org/httpcomponents-client)')
assert content.contains('(Apache License) HttpClient Cache (org.apache.httpcomponents:httpclient-cache:4.1.2 - http://hc.apache.org/httpcomponents-client)')
assert content.contains('(Apache License) HttpCore (org.apache.httpcomponents:httpcore:4.1.2 - http://hc.apache.org/httpcomponents-core-ga)')

return true;
