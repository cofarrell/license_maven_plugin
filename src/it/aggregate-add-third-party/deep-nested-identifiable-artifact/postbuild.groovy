/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

file = new File(basedir, 'target/generated-sources/license/third.txt');
assert file.exists();
content = file.text;
assert !content.contains('the project has no dependencies.');
assert content.contains('(Apache 2) Apache Velocity (com.atlassian.bundles:velocity:1.6.2-1 - http://www.atlassian.com/atlassian-public-pom/parent-pom/velocity/)')
assert content.contains('(BSD) Atlassian Plugins - OSGi bridge framework bundle (com.atlassian.plugins:atlassian-plugins-osgi-bridge:2.13.0-m10 - http://docs.atlassian.com/atlassian-plugins-osgi-bridge/2.13.0-m10/atlassian-plugins-osgi-bridge)')
assert content.contains('(BSD) Atlassian Plugins - OSGi bridge framework bundle (com.atlassian.plugins:atlassian-plugins-osgi-bridge:2.13.0 - http://docs.atlassian.com/atlassian-plugins-osgi-bridge/2.13.0/atlassian-plugins-osgi-bridge)')
assert content.contains('(BSD) Atlassian Plugins - Spring DM extender repackaged bundle (com.atlassian.plugins:atlassian-plugins-osgi-spring-extender:2.13.0-m10 - http://docs.atlassian.com/atlassian-plugins-osgi-spring-extender/2.13.0-m10/atlassian-plugins-osgi-spring-extender)')
assert content.contains('(BSD) Atlassian Plugins - Spring DM extender repackaged bundle (com.atlassian.plugins:atlassian-plugins-osgi-spring-extender:2.13.0 - http://docs.atlassian.com/atlassian-plugins-osgi-spring-extender/2.13.0/atlassian-plugins-osgi-spring-extender)')
assert content.contains('(MIT License) JCL 1.1.1 implemented over SLF4J (org.slf4j:jcl-over-slf4j:1.6.4 - http://www.slf4j.org)')
assert content.contains('(The Apache Software License, Version 2.0) Spring Framework (org.springframework:spring:2.5.6.SEC02 - http://www.springframework.org)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi Annotations (org.springframework.osgi:spring-osgi-annotation:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi Core (org.springframework.osgi:spring-osgi-core:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi Extender (org.springframework.osgi:spring-osgi-extender:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi IO (org.springframework.osgi:spring-osgi-io:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(BSD) Atlassian Plugins - OSGi Loader (com.atlassian.plugins:atlassian-plugins-osgi:2.13.0-m10 - http://docs.atlassian.com/atlassian-plugins-osgi/2.13.0-m10/atlassian-plugins-osgi)')

file = new File(basedir, 'target/generated-sources/license/test/third.txt');
assert file.exists();
content = file.text;
assert !content.contains('the project has no dependencies.');
assert content.contains('(Apache 2) Apache Velocity (com.atlassian.bundles:velocity:1.6.2-1 - http://www.atlassian.com/atlassian-public-pom/parent-pom/velocity/)')
assert content.contains('(BSD) Atlassian Plugins - OSGi bridge framework bundle (com.atlassian.plugins:atlassian-plugins-osgi-bridge:2.13.0-m10 - http://docs.atlassian.com/atlassian-plugins-osgi-bridge/2.13.0-m10/atlassian-plugins-osgi-bridge)')
assert content.contains('(BSD) Atlassian Plugins - OSGi bridge framework bundle (com.atlassian.plugins:atlassian-plugins-osgi-bridge:2.13.0 - http://docs.atlassian.com/atlassian-plugins-osgi-bridge/2.13.0/atlassian-plugins-osgi-bridge)')
assert content.contains('(BSD) Atlassian Plugins - Spring DM extender repackaged bundle (com.atlassian.plugins:atlassian-plugins-osgi-spring-extender:2.13.0-m10 - http://docs.atlassian.com/atlassian-plugins-osgi-spring-extender/2.13.0-m10/atlassian-plugins-osgi-spring-extender)')
assert content.contains('(BSD) Atlassian Plugins - Spring DM extender repackaged bundle (com.atlassian.plugins:atlassian-plugins-osgi-spring-extender:2.13.0 - http://docs.atlassian.com/atlassian-plugins-osgi-spring-extender/2.13.0/atlassian-plugins-osgi-spring-extender)')
assert content.contains('(MIT License) JCL 1.1.1 implemented over SLF4J (org.slf4j:jcl-over-slf4j:1.6.4 - http://www.slf4j.org)')
assert content.contains('(The Apache Software License, Version 2.0) Spring Framework (org.springframework:spring:2.5.6.SEC02 - http://www.springframework.org)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi Annotations (org.springframework.osgi:spring-osgi-annotation:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi Core (org.springframework.osgi:spring-osgi-core:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi Extender (org.springframework.osgi:spring-osgi-extender:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(Apache License, Version 2.0) Spring OSGi IO (org.springframework.osgi:spring-osgi-io:1.2.1 - http://www.springframework.org/osgi)')
assert content.contains('(BSD) Atlassian Plugins - OSGi Loader (com.atlassian.plugins:atlassian-plugins-osgi:2.13.0-m10 - http://docs.atlassian.com/atlassian-plugins-osgi/2.13.0-m10/atlassian-plugins-osgi)')

return true;
