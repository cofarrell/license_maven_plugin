package org.codehaus.mojo.license.api;

import junit.framework.TestCase;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.manager.WagonConfigurationException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.wagon.UnsupportedProtocolException;
import org.codehaus.mojo.license.utils.ArtifactCreator;
import org.codehaus.plexus.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.InputStream;
import java.util.Set;

import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link org.codehaus.mojo.license.api.DefaultAtlassianPluginHelper}
 */

@RunWith(MockitoJUnitRunner.class)
public class DefaultAtlassianPluginHelperTest extends TestCase
{

    private DefaultAtlassianPluginHelper atlassianPluginHelper;

    @Mock
    private ArtifactResolver artifactResolver;

    @Mock
    private ArtifactCreator artifactCreator;

    @Mock
    private Logger logger;

    @Before
    public void setUp() throws UnsupportedProtocolException, WagonConfigurationException
    {
        this.atlassianPluginHelper = new DefaultAtlassianPluginHelper(artifactResolver, artifactCreator);
        this.atlassianPluginHelper.enableLogging(logger);
    }


    @Test
    public void testResolveNoNestedArtifacts() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.licensetest", "nonestedartifact", "1.0-SNAPSHOT", null);
        final File noNestedArtifactJar = new File(getClass().getResource("nonestedartifact-1.0-SNAPSHOT.jar").getFile());
        when(mockArtifact.getFile()).thenReturn(noNestedArtifactJar);


        final Set<Artifact> resolvedArtifacts = atlassianPluginHelper.resolveNestedArtifacts(Sets.newSet(mockArtifact), null, null, false);
        assertThat(resolvedArtifacts, hasItem(mockArtifact));
        assertEquals(resolvedArtifacts.size(), 1);
    }

    @Test
    public void testResolveOneNestedArtifact() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.licensetest", "onenestedartifact", "1.0-SNAPSHOT", null);
        final File oneNestedArtifactJar = new File(getClass().getResource("onenestedartifact-1.0-SNAPSHOT.jar").getFile());
        final Artifact nestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");

        when(mockArtifact.getFile()).thenReturn(oneNestedArtifactJar);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-io-2.4.jar")))
            .thenReturn(nestedArtifact);

        final Set<Artifact> resolvedArtifacts = atlassianPluginHelper.resolveNestedArtifacts(Sets.newSet(mockArtifact), null, null, false);

        assertThat(resolvedArtifacts, hasItems(nestedArtifact, mockArtifact));
        assertEquals(resolvedArtifacts.size(), 2);
    }

    @Test
    public void testResolveMultipleNestedArtifacts() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.licensetest", "multiplenestedartifact", "1.0-SNAPSHOT", null);
        final File multipleNestedArtifactJar = new File(getClass().getResource("multiplenestedartifact-1.0-SNAPSHOT.jar").getFile());
        final Artifact nestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");
        final Artifact nestedArtifact2 = createMockArtifact("expectedGroup2", "expectedArtifact2", "expectedVersion2", "expectedClassifier2");

        when(mockArtifact.getFile()).thenReturn(multipleNestedArtifactJar);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-io-2.4.jar")))
                .thenReturn(nestedArtifact);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-math-2.2.jar")))
                .thenReturn(nestedArtifact2);

        final Set<Artifact> resolvedArtifacts = atlassianPluginHelper.resolveNestedArtifacts(Sets.newSet(mockArtifact), null, null, false);

        assertThat(resolvedArtifacts, hasItems(nestedArtifact, nestedArtifact2, mockArtifact));
        assertEquals(resolvedArtifacts.size(), 3);
    }


    @Test
    public void testResolveOneDeeplyNestedArtifact() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.licensetest", "onedeeplynestedartifact", "1.0-SNAPSHOT", null);
        final File oneDeeplyNestedArtifactJar = new File(getClass().getResource("onedeeplynestedartifact-1.0-SNAPSHOT.jar").getFile());
        final Artifact deeplyNestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");
        final Artifact nestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");

        when(mockArtifact.getFile()).thenReturn(oneDeeplyNestedArtifactJar);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-io-2.4.jar")))
                .thenReturn(deeplyNestedArtifact);
        when(artifactCreator.fromPom(any(InputStream.class), eq("onenestedartifact-1.0-SNAPSHOT.jar")))
                .thenReturn(nestedArtifact);

        final Set<Artifact> resolvedArtifacts = atlassianPluginHelper.resolveNestedArtifacts(Sets.newSet(mockArtifact), null, null, false);

        assertThat(resolvedArtifacts, hasItems(deeplyNestedArtifact, nestedArtifact, mockArtifact));
        assertEquals(resolvedArtifacts.size(), 3);
    }

    @Test
    public void testResolveMultipleDeeplyNestedArtifacts() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.licensetest", "multipledeeplynestedartifacts", "1.0-SNAPSHOT", null);
        final File multipleNestedArtifactJar = new File(getClass().getResource("multipledeeplynestedartifacts-1.0-SNAPSHOT.jar").getFile());
        final Artifact deeplyNestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");
        final Artifact nestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");
        final Artifact nestedArtifact2 = createMockArtifact("expectedGroup2", "expectedArtifact2", "expectedVersion2", "expectedClassifier2");

        when(mockArtifact.getFile()).thenReturn(multipleNestedArtifactJar);
        when(artifactCreator.fromPom(any(InputStream.class), eq("multiplenestedartifact-1.0-SNAPSHOT.jar")))
                .thenReturn(nestedArtifact);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-io-2.4.jar")))
                .thenReturn(deeplyNestedArtifact);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-math-2.2.jar")))
                .thenReturn(nestedArtifact2);

        final Set<Artifact> resolvedArtifacts = atlassianPluginHelper.resolveNestedArtifacts(Sets.newSet(mockArtifact), null, null, false);

        assertThat(resolvedArtifacts, hasItems(deeplyNestedArtifact, nestedArtifact, nestedArtifact2, mockArtifact));
        assertEquals(resolvedArtifacts.size(), 4);
    }

    @Test
    public void testNestedUnknownArtifacts() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.licensetest", "onenestedartifact", "1.0-SNAPSHOT", null);
        final File oneNestedArtifactJar = new File(getClass().getResource("onenestedartifact-1.0-SNAPSHOT.jar").getFile());
        final Artifact nestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");

        when(mockArtifact.getFile()).thenReturn(oneNestedArtifactJar);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-io-2.4.jar")))
                .thenReturn(nestedArtifact);

        final Set<Artifact> resolvedArtifacts = atlassianPluginHelper.resolveNestedArtifacts(Sets.newSet(mockArtifact), null, null, false);

        assertThat(resolvedArtifacts, hasItems(nestedArtifact, mockArtifact));
        assertEquals(resolvedArtifacts.size(), 2);
    }

    @Test
    public void testDeeplyNestedUnknownArtifacts() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.licensetest", "multipledeeplynestedartifacts", "1.0-SNAPSHOT", null);
        final File multipleNestedArtifactJar = new File(getClass().getResource("multipledeeplynestedartifacts-1.0-SNAPSHOT.jar").getFile());
        final Artifact deeplyNestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");
        final Artifact nestedArtifact = createMockArtifact("expectedGroup", "expectedArtifact", "expectedVersion", "expectedClassifier");
        final Artifact nestedArtifact2 = createMockArtifact("expectedGroup2", "expectedArtifact2", "expectedVersion2", "expectedClassifier2");

        when(mockArtifact.getFile()).thenReturn(multipleNestedArtifactJar);
        when(artifactCreator.fromPom(any(InputStream.class), eq("multiplenestedartifact-1.0-SNAPSHOT.jar")))
                .thenReturn(nestedArtifact);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-io-2.4.jar")))
                .thenReturn(deeplyNestedArtifact);
        when(artifactCreator.fromPom(any(InputStream.class), eq("commons-math-2.2.jar")))
                .thenReturn(nestedArtifact2);

        final Set<Artifact> resolvedArtifacts = atlassianPluginHelper.resolveNestedArtifacts(Sets.newSet(mockArtifact), null, null, false);

        assertThat(resolvedArtifacts, hasItems(deeplyNestedArtifact, nestedArtifact, nestedArtifact2, mockArtifact));
        assertEquals(resolvedArtifacts.size(), 4);
    }

    @Test
    public void testIsNotAtlassianArtifact() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.not.atlassian", "some-artifact", "3.0", null);
        assertFalse(atlassianPluginHelper.isAtlassianArtifact(mockArtifact));

        final Artifact mockArtifact2 = createMockArtifact("com.atlass", "some-artifact", "3.0", null);
        assertFalse(atlassianPluginHelper.isAtlassianArtifact(mockArtifact2));

    }

    @Test
    public void testIsAtlassianArtifact() throws Exception
    {
        final Artifact mockArtifact = createMockArtifact("com.atlassian.stuff", "some-artifact", "3.0", null);
        assertTrue(atlassianPluginHelper.isAtlassianArtifact(mockArtifact));

        final Artifact mockArtifact2 = createMockArtifact("com.atlassian", "some-artifact", "3.0", null);
        assertTrue(atlassianPluginHelper.isAtlassianArtifact(mockArtifact2));

        final Artifact mockArtifact3 = createMockArtifact("com.atlassian", "some-artifact", "3.0-SNAPSHOT", null);
        assertTrue(atlassianPluginHelper.isAtlassianArtifact(mockArtifact3));
    }

    private Artifact createMockArtifact(final String groupId, final String artifactId, final String version,
                                        final String classifier)
    {
        final Artifact mockArtifact = mock(Artifact.class);
        when(mockArtifact.getGroupId()).thenReturn(groupId);
        when(mockArtifact.getArtifactId()).thenReturn(artifactId);
        when(mockArtifact.getVersion()).thenReturn(version);
        when(mockArtifact.getClassifier()).thenReturn(classifier);
        return mockArtifact;
    }
}
